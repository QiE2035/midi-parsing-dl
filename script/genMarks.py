import bpy,json

data = json.loads(bpy.data.texts['data'].as_string())
fps = bpy.context.scene.render.fps/bpy.context.scene.render.fps_base
idx = 1
for i in data:
    bpy.context.scene.timeline_markers.new(str(idx),frame=int(i*fps))
    idx += 1