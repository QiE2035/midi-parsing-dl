import bpy

speed = 10

render = bpy.context.scene.render
fps = render.fps/render.fps_base
turn = True
pre_point = 1
points = []
add_cube = bpy.ops.mesh.primitive_cube_add
cube = bpy.context.object
location = cube.location

for mark in bpy.context.scene.timeline_markers:
    points.append(mark.frame)
    pass
points.sort()


def ci(f):
    cube.keyframe_insert("location",frame=f)
ci(1)
for point in points:
    step = ((point-pre_point)/fps)*speed
    add_cube(location=location)
    obj = bpy.context.object
    obj.name = "Line"
    ol = obj.location
    os = obj.scale
    oi=obj.keyframe_insert
    print(os)
    oi("scale",frame=pre_point)
    oi("location",frame=pre_point)
    if turn:
        ol.x += step/2
        location.x += step
        os.x += step/2
        pass
    else:
        ol.y += step/2
        location.y += step 
        os.y += step/2
        pass
    oi("scale",frame=point)
    os.xy = (0,0)
    oi("scale",frame=pre_point-1)
    oi("location",frame=point)
    ci(point)
    turn = not turn
    pre_point = point
    