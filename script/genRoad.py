import bpy

speed = 10
width = 2

render = bpy.context.scene.render
fps = render.fps/render.fps_base
turn = True
pre_point = 1
points = []
add_cube = bpy.ops.mesh.primitive_cube_add
location = [0,0,-2]

for mark in bpy.context.scene.timeline_markers:
    points.append(mark.frame)
    pass
points.sort()

for point in points:
    step = ((point-pre_point)/fps)*speed
    add_cube(location=location)
    obj = bpy.context.object
    obj.name = "Road"
    ol = obj.location
    os = obj.scale
    if turn:
        ol.x += step/2
        location[0] += step 
        os.y = width
        os.x += (step+width)/2
        pass
    else:
        ol.y += step/2
        location[1] += step 
        os.y += (step+width)/2
        os.x = width
        pass
    turn = not turn
    pre_point = point
    
